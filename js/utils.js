export function lengthFormatStr(seconds) {
  const mins = Math.floor(seconds / 60);
  const secs = Math.floor(seconds - mins * 60);

  return (secs === 60 ? `${mins + 1}:00` : `${mins}:${secs < 10 ? '0' : ''}${secs}`);
}

export function getResource(path) {
  return `../resources/${path}`;
}
